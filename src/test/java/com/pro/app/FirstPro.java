package com.pro.app;

import java.io.IOException;
import com.pro.app.Base_Setup;

import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FirstPro extends Base_Setup {
	
	@Test(dataProvider = "getData")
	public void scribd(String username, String password) throws IOException, InterruptedException {
		
		System.out.println("this is the script to be runned");
		
		driver = Initializer();
		driver.get(prop.getProperty("url"));
		
		Landing_Page lp = new Landing_Page(driver);
		lp.getLogin().click();
		Login_Page lp1 = new Login_Page(driver);
		lp1.getuserName().sendKeys(username);
		lp1.getPassword().sendKeys(password);
		lp1.getlogIn().click();
		
	}
	
	@DataProvider
	public Object[][] getData() {
		
		Object[][] data = new Object[1][2];
		
		data[0][0] = "wronguser@gmail.com"; 
		data[0][1] = "wrongpassword";
		
//		data[1][0] = "rightuser@gmail.com"; 
//		data[1][1] = "rightpassword";
		
		return data;
	}
	
	@AfterTest
	public void teardowne() {
		driver.close();
		driver=null; //it is used to avoid to keep driver alive inorder to avoid memory leaks/waste
	}

}

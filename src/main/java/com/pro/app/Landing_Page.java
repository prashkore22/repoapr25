package com.pro.app;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Landing_Page {
	
	public WebDriver driver;
	
	By signIn = By.cssSelector("a[href*='sign_in']");
	
	public Landing_Page(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement getLogin() {
		
		return driver.findElement(signIn);
	}

}

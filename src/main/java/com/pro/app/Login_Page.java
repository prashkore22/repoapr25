package com.pro.app;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login_Page {
	
	public WebDriver driver;
	
	By userName = By.xpath("//input[@type='email']");
	By password = By.xpath("//input[@type='password']");
	By logIn = By.xpath("//input[@type='submit']");
	
	public Login_Page(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement getuserName() {
		
		return driver.findElement(userName);
	}
	
    public WebElement getPassword() {
		
		return driver.findElement(password);
	}
    
    public WebElement getlogIn() {
		
		return driver.findElement(logIn);
	}

}

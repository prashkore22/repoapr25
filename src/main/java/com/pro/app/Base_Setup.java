package com.pro.app;


import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class Base_Setup {
	
	public static WebDriver driver;
	public Properties prop;
	public WebDriver Initializer() throws IOException {
		
		prop = new Properties();
		FileInputStream file = new FileInputStream("C:\\Users\\prashanth.kore\\pro-app\\src\\main\\java\\com\\pro\\app\\data.properties");
		prop.load(file);
		String browserName = prop.getProperty("browser");
		
		if(browserName.equals("chrome")) {
			//chrome code goes here
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\prashanth.kore\\Desktop\\sample learning jav\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		else if(browserName.equals("Firefox")) {
			//Firefox code goes here
		}
		else if(browserName.equals("IE")){
			//IE code goes here
		}
		else {
			//enter correct browser
		}
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		return driver;
	}
	
	public void getScreenshot(String module) throws IOException {
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("D:\\Eclipse\\test_screen\\"+module+"sample.png"));
	}

}
